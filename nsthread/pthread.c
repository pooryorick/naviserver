/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 * http://mozilla.org/.
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See
 * the License for the specific language governing rights and limitations
 * under the License.
 *
 * The Original Code is AOLserver Code and related documentation
 * distributed by AOL.
 *
 * The Initial Developer of the Original Code is America Online,
 * Inc. Portions created by AOL are Copyright (C) 1999 America Online,
 * Inc. All Rights Reserved.
 *
 * Alternatively, the contents of this file may be used under the terms
 * of the GNU General Public License (the "GPL"), in which case the
 * provisions of GPL are applicable instead of those above.  If you wish
 * to allow use of your version of this file only under the terms of the
 * GPL and not to allow others to use your version of this file under the
 * License, indicate your decision by deleting the provisions above and
 * replace them with the notice and other provisions required by the GPL.
 * If you do not delete the provisions above, a recipient may use your
 * version of this file under either the License or the GPL.
 */

#ifndef _WIN32
#include <nsconfig.h>
#endif

#ifdef HAVE_PTHREAD

/*
 * pthread.c --
 *
 *      Interface routines for nsthreads using pthreads.
 *
 */

#include "thread.h"
#include <pthread.h>

/*
 * Local functions defined in this file.
 */

static pthread_cond_t *GetCond(Ns_Cond *cond)   NS_GNUC_NONNULL(1) NS_GNUC_RETURNS_NONNULL;
static void CleanupTls(void *arg)               NS_GNUC_NONNULL(1);
static void *ThreadMain(void *arg);

/*
 * Solaris has weird way to declare this so just do more concisely what the
 * (solaris) definition really does.
 */

#if defined(__sun__)
#define PTHREAD_STACK_MIN  ((size_t)sysconf(_SC_THREAD_STACK_MIN))
#endif

/*
 * This single TLS key is used to store the nsthread
 * TLS slots. Due to system limitation(s), we stuff all of the
 * slots into a private array keyed onto this per-thread key,
 * instead of using separate TLS keys for each consumer.
 */

static pthread_key_t key;


/*
 *----------------------------------------------------------------------
 *
 * Nsthreads_LibInit --
 *
 *      Pthread library initialization routine.
 *
 * Results:
 *      None.
 *
 * Side effects:
 *      Creates pthread key.
 *
 *----------------------------------------------------------------------
 */

void
Nsthreads_LibInit(void)
{
    static bool initialized = NS_FALSE;

    if (!initialized) {
        int err;

        initialized = NS_TRUE;
#ifdef __linux
        {
            size_t n;

            n = confstr(_CS_GNU_LIBPTHREAD_VERSION, NULL, 0);
            if (n > 0) {
                char *buf = ns_malloc(n);

                confstr(_CS_GNU_LIBPTHREAD_VERSION, buf, n);
                if (!strstr (buf, "NPTL")) {
                    Tcl_Panic("Linux \"NPTL\" thread library required. Found: \"%s\"", buf);
                }
                ns_free(buf);
            }
        }
#endif
        err = pthread_key_create(&key, CleanupTls);
        if (err != 0) {
            NsThreadFatal("Nsthreads_LibInit", "pthread_key_create", err);
        }
        NsInitThreads();
    }
}


/*
 *----------------------------------------------------------------------
 *
 * NsGetTls --
 *
 *      Returns the TLS slots.
 *
 * Results:
 *      A pointer to the slots array.
 *
 * Side effects:
 *      Storage for the slot array is allocated bypassing the
 *      currently-configured memory allocator because at the
 *      time this storage is to be reclaimed (see: CleanupTls)
 *      the allocator may already be finalized for this thread.
 *
 *----------------------------------------------------------------------
 */

void **
NsGetTls(void)
{
    void **slots;

    slots = pthread_getspecific(key);
    if (slots == NULL) {
        slots = calloc(NS_THREAD_MAXTLS, sizeof(void *));
        if (slots == NULL) {
            fprintf(stderr, "Fatal: NsGetTls failed to allocate %" PRIuz " bytes.\n",
                    NS_THREAD_MAXTLS * sizeof(void *));
            abort();
        }
        pthread_setspecific(key, slots);
    }
    return slots;
}


/*
 *----------------------------------------------------------------------
 *
 * NsThreadLibName --
 *
 *      Returns the name of the thread library.
 *
 * Results:
 *      A pointer to constant string.
 *
 * Side effects:
 *      None.
 *
 *----------------------------------------------------------------------
 */

const char *
NsThreadLibName(void)
{
    return "pthread";
}


/*
 *----------------------------------------------------------------------
 *
 * NsLockAlloc --
 *
 *      Allocates and initializes a mutex lock.
 *
 * Results:
 *      None.
 *
 * Side effects:
 *      None.
 *
 *----------------------------------------------------------------------
 */

void *
NsLockAlloc(void)
{
    pthread_mutex_t *lock;
    int err;

    lock = ns_malloc(sizeof(pthread_mutex_t));
    err = pthread_mutex_init(lock, NULL);
    if (err != 0) {
        NsThreadFatal("NsLockAlloc", "pthread_mutex_init", err);
    }
    return lock;
}


/*
 *----------------------------------------------------------------------
 *
 * NsLockFree --
 *
 *      Frees a mutex lock.
 *
 * Results:
 *      None.
 *
 * Side effects:
 *      None.
 *
 *----------------------------------------------------------------------
 */

void
NsLockFree(void *lock)
{
    int err;

    NS_NONNULL_ASSERT(lock != NULL);

    err = pthread_mutex_destroy((pthread_mutex_t *) lock);
    if (err != 0) {
        NsThreadFatal("NsLockFree", "pthread_mutex_destroy", err);
    }
    ns_free(lock);
}


/*
 *----------------------------------------------------------------------
 *
 * NsLockSet --
 *
 *      Sets a mutex lock.
 *
 * Results:
 *      None.
 *
 * Side effects:
 *      May wait for the lock to become available.
 *
 *----------------------------------------------------------------------
 */

void
NsLockSet(void *lock)
{
    int err;

    NS_NONNULL_ASSERT(lock != NULL);

    err = pthread_mutex_lock((pthread_mutex_t *) lock);
    if (err != 0) {
        NsThreadFatal("NsLockSet", "pthread_mutex_lock", err);
    }
}


/*
 *----------------------------------------------------------------------
 *
 * NsLockTry --
 *
 *      Tries once to set a mutex lock.
 *
 * Results:
 *      NS_TRUE if lock set, NS_FALSE otherwise.
 *
 * Side effects:
 *      None.
 *
 *----------------------------------------------------------------------
 */

bool
NsLockTry(void *lock)
{
    int err;

    NS_NONNULL_ASSERT(lock != NULL);

    err = pthread_mutex_trylock((pthread_mutex_t *) lock);
    if (unlikely(err == EBUSY)) {
        return NS_FALSE;
    }
    if (unlikely(err != 0)) {
        NsThreadFatal("NsLockTry", "pthread_mutex_trylock", err);
    }

    return NS_TRUE;
}


/*
 *----------------------------------------------------------------------
 *
 * NsLockUnset --
 *
 *      Unsets a mutex lock.
 *
 * Results:
 *      None.
 *
 * Side effects:
 *      May wait for the lock to become available.
 *
 *----------------------------------------------------------------------
 */

void
NsLockUnset(void *lock)
{
    int err;

    NS_NONNULL_ASSERT(lock != NULL);

    err = pthread_mutex_unlock((pthread_mutex_t *) lock);
    if (unlikely(err != 0)) {
        NsThreadFatal("NsLockUnset", "pthread_mutex_unlock", err);
    }
}


/*
 *----------------------------------------------------------------------
 *
 * NsCreateThread --
 *
 *      Pthread-specific thread create function called by
 *      Ns_ThreadCreate.
 *
 * Results:
 *      None.
 *
 * Side effects:
 *      Depends on thread startup routine.
 *
 *----------------------------------------------------------------------
 */

void
NsCreateThread(void *arg, ssize_t stacksize, Ns_Thread *threadPtr)
{
    static const char *func = "NsCreateThread";
    pthread_attr_t     attr;
    pthread_t          thr;
    int                err;

    err = pthread_attr_init(&attr);
    if (err != 0) {
        NsThreadFatal(func, "pthread_attr_init", err);
    }

    /*
     * Set the stack size if specified explicitly.  It is smarter
     * to leave the default on platforms which map large stacks
     * with guard zones (e.g., Solaris and Linux).
     */

    if (stacksize > 0) {
        if (stacksize < PTHREAD_STACK_MIN) {
            stacksize = PTHREAD_STACK_MIN;
        } else {
          /*
           * Make the stack size a multiple of the page-size
           * so that pthread_attr_setstacksize does not fail. When 
           * _SC_PAGESIZE is defined, try to be friendly and round the
           * stack size to the next multiple of the page-size.
           */
#if defined(_SC_PAGESIZE)
            long pageSize = sysconf(_SC_PAGESIZE);
            stacksize = (((stacksize-1) / pageSize) + 1) * pageSize;
#endif
        }
        err = pthread_attr_setstacksize(&attr, (size_t) stacksize);
        if (err != 0) {
            NsThreadFatal(func, "pthread_attr_setstacksize", err);
        }
    }

    /*
     * System scope always preferred. Ignore any unsupported error.
     */
    err = pthread_attr_setscope(&attr, PTHREAD_SCOPE_SYSTEM);
    if (err != 0 && err != ENOTSUP) {
        NsThreadFatal(func, "pthread_setscope", err);
    }

    if (threadPtr == NULL) {
	/*
	 * No threadPtr was given. Create a detached thread.
	 */
        err = pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
        if (err != 0 && err != ENOTSUP) {
            NsThreadFatal(func, "pthread_setdetachstate", err);
        }
    }

    /*
     * Create the workhorse thread
     */
    err = pthread_create(&thr, &attr, ThreadMain, arg);
    if (err != 0) {
        NsThreadFatal(func, "pthread_create", err);
    } else if (threadPtr != NULL) {
        *threadPtr = (Ns_Thread)(uintptr_t) thr;
    }

    /*
     *
     */
    err = pthread_attr_destroy(&attr);
    if (err != 0) {
        NsThreadFatal(func, "pthread_attr_destroy", err);
    }
}


/*
 *----------------------------------------------------------------------
 *
 * NsThreadExit --
 *
 *      Terminates a thread.
 *
 * Results:
 *      None.
 *
 * Side effects:
 *      Thread cleans itself up via the TLS cleanup code.
 *
 *----------------------------------------------------------------------
 */

void
NsThreadExit(void *arg)
{
   /*
    * Really exit the thread. This invokes all the
    * registered TLS cleanup callbacks again (no harm).
    */

    pthread_exit(arg);
}

/*
 *----------------------------------------------------------------------
 *
 * NsThreadResult --
 *
 *      Stub function, which is not necessary when pthreads are used, since
 *      pthread_exit passes a pointer values). However, the situation for
 *      windows is different, and we keep this function here for symmetry with
 *      the version using windows native threads. For background, see
 *      winthread.c.
 *
 * Results:
 *      Pointer value (can be NULL).
 *
 * Side effects:
 *      None.
 *
 *----------------------------------------------------------------------
 */
void *
NsThreadResult(void *arg)
{
    return arg;
}


/*
 *----------------------------------------------------------------------
 *
 * Ns_ThreadJoin --
 *
 *      Waits for an attached thread to exit.
 *
 * Results:
 *      None.
 *
 * Side effects:
 *      Destroys the thread after joining it.
 *
 *----------------------------------------------------------------------
 */

void
Ns_ThreadJoin(Ns_Thread *thread, void **argPtr)
{
    int err;

    NS_NONNULL_ASSERT(thread != NULL);

    err = pthread_join((pthread_t)(uintptr_t)*thread, argPtr);
    if (err != 0) {
        NsThreadFatal("Ns_ThreadJoin", "pthread_join", err);
    }
}


/*
 *----------------------------------------------------------------------
 *
 * Ns_ThreadYield --
 *
 *      Yields the CPU to another thread.
 *
 * Results:
 *      None.
 *
 * Side effects:
 *      See sched_yield().
 *
 *----------------------------------------------------------------------
 */

void
Ns_ThreadYield(void)
{
    sched_yield();
}


/*
 *----------------------------------------------------------------------
 *
 * Ns_ThreadId --
 *
 *      Returns the numeric thread id.
 *
 * Results:
 *      Integer thread id.
 *
 * Side effects:
 *      None.
 *
 *----------------------------------------------------------------------
 */

uintptr_t
Ns_ThreadId(void)
{
    pthread_t result = pthread_self();

    return (uintptr_t) result;
}


/*
 *----------------------------------------------------------------------
 *
 * Ns_ThreadSelf --
 *
 *      Returns a handle to the current thread suitable for Ns_ThreadJoin.
 *
 * Results:
 *      None.
 *
 * Side effects:
 *      Stores a handle to the thread at the address give by threadPtr.
 *
 *----------------------------------------------------------------------
 */

void
Ns_ThreadSelf(Ns_Thread *threadPtr)
{
    pthread_t result = pthread_self();

    NS_NONNULL_ASSERT(threadPtr != NULL);

    *threadPtr = (Ns_Thread)result;
}


/*
 *----------------------------------------------------------------------
 *
 * Ns_CondInit --
 *
 *      Initializes a Pthread condition variable.  This routine
 *      isn't used directly very often as static condition variables
 *      self-initialize on first use.
 *
 * Results:
 *      None.
 *
 * Side effects:
 *      None.
 *
 *----------------------------------------------------------------------
 */

void
Ns_CondInit(Ns_Cond *cond)
{
    pthread_cond_t *condPtr;
    int             err;

    NS_NONNULL_ASSERT(cond != NULL);

    condPtr = ns_malloc(sizeof(pthread_cond_t));
    err = pthread_cond_init(condPtr, NULL);
    if (err != 0) {
        NsThreadFatal("Ns_CondInit", "pthread_cond_init", err);
    }
    *cond = (Ns_Cond) condPtr;
}


/*
 *----------------------------------------------------------------------
 *
 * Ns_CondDestroy --
 *
 *      Destroys a Pthread condition.  This routine is almost never
 *      used as condition variables normally exist in memory until
 *      the process exits.
 *
 * Results:
 *      None.
 *
 * Side effects:
 *      None.
 *
 *----------------------------------------------------------------------
 */

void
Ns_CondDestroy(Ns_Cond *cond)
{
    pthread_cond_t *condPtr = (pthread_cond_t *) *cond;

    if (condPtr != NULL) {
        int err;

        err = pthread_cond_destroy(condPtr);
        if (err != 0) {
            NsThreadFatal("Ns_CondDestroy", "pthread_cond_destroy", err);
        }
        ns_free(condPtr);
        *cond = NULL;
    }
}


/*
 *----------------------------------------------------------------------
 *
 * Ns_CondSignal --
 *
 *      Signals a Pthread condition.
 *
 * Results:
 *      None.
 *
 * Side effects:
 *      See pthread_cond_signal.
 *
 *----------------------------------------------------------------------
 */

void
Ns_CondSignal(Ns_Cond *cond)
{
    int             err;

    NS_NONNULL_ASSERT(cond != NULL);

    err = pthread_cond_signal(GetCond(cond));
    if (err != 0) {
        NsThreadFatal("Ns_CondSignal", "pthread_cond_signal", err);
    }
}


/*
 *----------------------------------------------------------------------
 *
 * Ns_CondBroadcast --
 *
 *      Broadcasts a Pthread condition.
 *
 * Results:
 *      None.
 *
 * Side effects:
 *      See pthread_cond_broadcast.
 *
 *----------------------------------------------------------------------
 */

void
Ns_CondBroadcast(Ns_Cond *cond)
{
    int err;

    NS_NONNULL_ASSERT(cond != NULL);

    err = pthread_cond_broadcast(GetCond(cond));
    if (err != 0) {
        NsThreadFatal("Ns_CondBroadcast", "pthread_cond_broadcast", err);
    }
}


/*
 *----------------------------------------------------------------------
 *
 * Ns_CondWait --
 *
 *      Waits indefinitely for a Pthread condition.
 *
 * Results:
 *      None.
 *
 * Side effects:
 *      See pthread_cond_wait.
 *
 *----------------------------------------------------------------------
 */

void
Ns_CondWait(Ns_Cond *cond, Ns_Mutex *mutex)
{
    int err;

    NS_NONNULL_ASSERT(cond != NULL);
    NS_NONNULL_ASSERT(mutex != NULL);

    err = pthread_cond_wait(GetCond(cond), NsGetLock(mutex));
    if (err != 0) {
        NsThreadFatal("Ns_CondWait", "pthread_cond_wait", err);
    }
}


/*
 *----------------------------------------------------------------------
 *
 * Ns_CondTimedWait --
 *
 *      Waits for a condition until the absolute time has passed.
 *
 * Results:
 *      None.
 *
 * Side effects:
 *      See pthread_cond_timewait.
 *
 *----------------------------------------------------------------------
 */

Ns_ReturnCode
Ns_CondTimedWait(Ns_Cond *cond, Ns_Mutex *mutex, const Ns_Time *timePtr)
{
    int              err;
    Ns_ReturnCode    status;
    struct timespec  ts;

    NS_NONNULL_ASSERT(cond != NULL);
    NS_NONNULL_ASSERT(mutex != NULL);

    if (timePtr == NULL) {
        Ns_CondWait(cond, mutex);
        return NS_OK;
    }

    /*
     * Convert the microsecond-based Ns_Time to a nanosecond-based
     * struct timespec.
     */

    ts.tv_sec = timePtr->sec;
    ts.tv_nsec = timePtr->usec * 1000;

    /*
     * As documented on Linux, pthread_cond_timedwait may return NS_EINTR if a
     * signal arrives.  We observed that Solaris may return NS_EINTR as well
     * although this is not documented.  Assume in this case that the wakeup is
     * spurious and simply restart the wait knowing that the ts structure has
     * not been modified.
     */

    do {
        err = pthread_cond_timedwait(GetCond(cond), NsGetLock(mutex), &ts);
    } while (err == NS_EINTR);
    if (err == ETIMEDOUT) {
        status = NS_TIMEOUT;
    } else if (err != 0) {
        NsThreadFatal("Ns_CondTimedWait", "pthread_cond_timedwait", err);
    } else {
        status = NS_OK;
    }
    return status;
}


/*
 *----------------------------------------------------------------------
 *
 * GetCond --
 *
 *      Casts an Ns_Cond to pthread_cond_t, initializing the condition if
 *      needed.
 *
 * Results:
 *      Pointer to pthread_cond_t.
 *
 * Side effects:
 *      Ns_Cond is initialized the first time.
 *
 *----------------------------------------------------------------------
 */

static pthread_cond_t *
GetCond(Ns_Cond *cond)
{
    NS_NONNULL_ASSERT(cond != NULL);

    if (*cond == NULL) {
        Ns_MasterLock();
        if (*cond == NULL) {
            Ns_CondInit(cond);
        }
        Ns_MasterUnlock();
    }
    return (pthread_cond_t *) *cond;
}


/*
 *----------------------------------------------------------------------
 *
 * ThreadMain --
 *
 *      Pthread startup routine.
 *
 * Results:
 *      Does not return.
 *
 * Side effects:
 *      NsThreadMain calls Ns_ThreadExit.
 *
 *----------------------------------------------------------------------
 */

static void *
ThreadMain(void *arg)
{
    NsThreadMain(arg);
    return NULL;
}


/*
 *----------------------------------------------------------------------
 *
 * CleanupTls --
 *
 *      Pthread TLS cleanup called during thread
 *      exit.  Might be called more than once if some
 *      other pthread cleanup requires nsthread's TLS.
 *
 * Results:
 *      None.
 *
 * Side effects:
 *      Bypasses the current memory allocator to reclam storage for the TLS
 *      slot array because the allocator may already be finalized for this
 *      thread.
 *
 *----------------------------------------------------------------------
 */

static void
CleanupTls(void *arg)
{
    void **slots = arg;
    Ns_Thread thread = NULL;

    NS_NONNULL_ASSERT(arg != NULL);

    /*
     * Restore the current slots during cleanup so handlers can access
     * TLS in other slots.
     */

    pthread_setspecific(key, arg);
    Ns_ThreadSelf(&thread);
    NsCleanupTls(slots);
    pthread_setspecific(key, NULL);
    free(slots);
}
#else
# ifndef _WIN32
#  error "pthread support is required"
# endif
#endif


/*
 * Local Variables:
 * mode: c
 * c-basic-offset: 4
 * fill-column: 78
 * indent-tabs-mode: nil
 * End:
 */
