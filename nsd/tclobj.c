/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 * http://mozilla.org/.
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See
 * the License for the specific language governing rights and limitations
 * under the License.
 *
 * The Original Code is AOLserver Code and related documentation
 * distributed by AOL.
 *
 * The Initial Developer of the Original Code is America Online,
 * Inc. Portions created by AOL are Copyright (C) 1999 America Online,
 * Inc. All Rights Reserved.
 *
 * Alternatively, the contents of this file may be used under the terms
 * of the GNU General Public License (the "GPL"), in which case the
 * provisions of GPL are applicable instead of those above.  If you wish
 * to allow use of your version of this file only under the terms of the
 * GPL and not to allow others to use your version of this file under the
 * License, indicate your decision by deleting the provisions above and
 * replace them with the notice and other provisions required by the GPL.
 * If you do not delete the provisions above, a recipient may use your
 * version of this file under either the License or the GPL.
 */


/*
 * tclobj.c --
 *
 *      Helper routines for managing Tcl_Obj types.
 */

#include "nsd.h"

/*
 * Local functions defined in this file.
 */

static Tcl_UpdateStringProc UpdateStringOfAddr;
static Tcl_SetFromAnyProc   SetAddrFromAny;

/*
 * Local variables defined in this file.
 */

static const Tcl_ObjType addrType = {
    "ns:addr",
    NULL,
    NULL,
    UpdateStringOfAddr,
    SetAddrFromAny
};

static const Tcl_ObjType *byteArrayTypePtr; /* For NsTclObjIsByteArray(). */
static const Tcl_ObjType *properByteArrayTypePtr;  /* For NsTclObjIsByteArray(). */

/*
 *----------------------------------------------------------------------
 *
 * NsTclInitAddrType --
 *
 *      Initialize the Tcl address object type and cache the bytearray Tcl
 *      built-in type. Starting with Tcl 8.7a1, Tcl has actually two different
 *      types for bytearrays, the old "tclByteArrayType" and a new
 *      "properByteArrayType", where both have the string name "bytearray".
 *
 * Results:
 *      None.
 *
 * Side effects:
 *      None.
 *
 *----------------------------------------------------------------------
 */

void
NsTclInitAddrType(void)
{
    Tcl_Obj *newByteObj;

    Tcl_RegisterObjType(&addrType);
    /*
     * Get the "tclByteArrayType" via name "bytearray".
     */
    byteArrayTypePtr = Tcl_GetObjType("bytearray");

    /*
     * Get the "properByteArrayType" via a TclObj.
     * In versions before Tcl 8.7, both values will be the same.
     */
    newByteObj = Tcl_NewByteArrayObj(NULL, 0);
    properByteArrayTypePtr = newByteObj->typePtr;
    if (properByteArrayTypePtr == byteArrayTypePtr) {
        /*
         * When both values are the same, we are in a Tcl version before 8.7,
         * where we have no properByteArrayTypePtr. So set it to an invalid
         * value to avoid potential confusions. Without this stunt, we would
         * need several ifdefs.
         */
        properByteArrayTypePtr = (Tcl_ObjType *)INT2PTR(0xffffff);
    }
    Tcl_DecrRefCount(newByteObj);
}


/*
 *----------------------------------------------------------------------
 *
 * Ns_TclResetObjType --
 *
 *      Reset the given Tcl_Obj type, freeing any type specific
 *      internal representation. The new Tcl_Obj type might be NULL.
 *
 * Results:
 *      None.
 *
 * Side effects:
 *      Depends on object type.
 *
 *----------------------------------------------------------------------
 */

void
Ns_TclResetObjType(Tcl_Obj *objPtr, const Tcl_ObjType *newTypePtr)
{
    const Tcl_ObjType *typePtr;

    NS_NONNULL_ASSERT(objPtr != NULL);

    typePtr = objPtr->typePtr;
    if (typePtr != NULL && typePtr->freeIntRepProc != NULL) {
        (*typePtr->freeIntRepProc)(objPtr);
    }
    objPtr->typePtr = newTypePtr;
}


/*
 *----------------------------------------------------------------------
 *
 * Ns_TclSetTwoPtrValue --
 *
 *      Resets the internal representation of a Tcl_Obj to the indicated type
 *      and stores ptr1 and ptr2 in twoPtrValue.
 *
 * Results:
 *      None.
 *
 * Side effects:
 *
 *----------------------------------------------------------------------
 */

void
Ns_TclSetTwoPtrValue(Tcl_Obj *objPtr, const Tcl_ObjType *newTypePtr,
                     void *ptr1, void *ptr2)
{
    NS_NONNULL_ASSERT(objPtr != NULL);

    Ns_TclResetObjType(objPtr, newTypePtr);
    objPtr->internalRep.twoPtrValue.ptr1 = ptr1;
    objPtr->internalRep.twoPtrValue.ptr2 = ptr2;
}


/*
 *----------------------------------------------------------------------
 *
 * Ns_TclSetOtherValuePtr --
 *
 *      Frees any existing internal representation and sets the object's
 *      object's type and otherValuePtr.
 *
 * Results:
 *      None.
 *
 * Side effects:
 *      Depends on object type.
 *
 *----------------------------------------------------------------------
 */

void
Ns_TclSetOtherValuePtr(Tcl_Obj *objPtr, const Tcl_ObjType *newTypePtr, void *value)
{
    NS_NONNULL_ASSERT(objPtr != NULL);
    NS_NONNULL_ASSERT(newTypePtr != NULL);
    NS_NONNULL_ASSERT(value != NULL);

    Ns_TclResetObjType(objPtr, newTypePtr);
    objPtr->internalRep.otherValuePtr = value;
}


/*
 *----------------------------------------------------------------------
 *
 * Ns_TclSetStringRep --
 *
 *      Copies length bytes and to the bytes member of a Tcl_Obj.  Does not
 *      free existing bytes.  Tcl uses "int" instead rather than "size", both
 *      internally and in its interface.
 *
 * Results:
 *      None.
 *
 * Side effects:
 *      Memory is allocated.
 *
 *----------------------------------------------------------------------
 */

void
Ns_TclSetStringRep(Tcl_Obj *objPtr, const char *bytes, int length)
{
    NS_NONNULL_ASSERT(objPtr != NULL);
    NS_NONNULL_ASSERT(bytes != NULL);

    if (length < 1) {
        length = (int)strlen(bytes);
    }
    objPtr->length = length;
    objPtr->bytes = ckalloc((unsigned) length + 1u);
    memcpy(objPtr->bytes, bytes, (size_t) length + 1u);
}



/*
 *----------------------------------------------------------------------
 *
 * Ns_TclSetFromAnyError --
 *
 *      Registered as the setFromAnyProc for an object type that doesn't
 *      support converting to the type from an arbitrary string representation.
 *
 * Results:
 *      TCL_ERROR.
 *
 * Side effects:
 *      Sets the interp's result to an error message if interp is not NULL.
 *
 *----------------------------------------------------------------------
 */

int
Ns_TclSetFromAnyError(Tcl_Interp *interp, Tcl_Obj *UNUSED(objPtr))
{
    Tcl_AppendToObj(Tcl_GetObjResult(interp),
                    "can't convert value to requested type except via prescribed API",
                    -1);
    return TCL_ERROR;
}


/*
 *----------------------------------------------------------------------
 *
 * Ns_TclGetAddrFromObj --
 *
 *      Stores at the address given by addrPtrPtr a pointer to the address
 *      internal representation of the Tcl_Obj. 
 *
 * Results:
 *      TCL_OK or TCL_ERROR if the Tcl_Obj could not be interpreted as an
 *      address type or if the address is not the right kind of address.
 *
 * Side effects:
 *      The internal representation of the Tcl_Obj is changed if needed.
 *
 *----------------------------------------------------------------------
 */

int
Ns_TclGetAddrFromObj(Tcl_Interp *interp, Tcl_Obj *objPtr,
                     const char *type, void **addrPtrPtr)
{
    int result = TCL_OK;

    NS_NONNULL_ASSERT(objPtr != NULL);
    NS_NONNULL_ASSERT(type != NULL);
    NS_NONNULL_ASSERT(addrPtrPtr != NULL);

    if (Tcl_ConvertToType(interp, objPtr, &addrType) != TCL_OK) {
        result = TCL_ERROR;

    } else if (objPtr->internalRep.twoPtrValue.ptr1 != (void *) type) {
        Ns_TclPrintfResult(interp, "incorrect type: %s", Tcl_GetString(objPtr));
        result = TCL_ERROR;

    } else {
        *addrPtrPtr = objPtr->internalRep.twoPtrValue.ptr2;
    }

    return result;
}


/*
 *----------------------------------------------------------------------
 *
 * Ns_TclSetAddrObj --
 *
 *      Converts the given object to the ns:addr type.
 *
 * Results:
 *      None.
 *
 * Side effects:
 *      None.
 *
 *----------------------------------------------------------------------
 */

void
Ns_TclSetAddrObj(Tcl_Obj *objPtr, const char *type, void *addr)
{
    NS_NONNULL_ASSERT(objPtr != NULL);
    NS_NONNULL_ASSERT(type != NULL);
    NS_NONNULL_ASSERT(addr != NULL);

    if (Tcl_IsShared(objPtr)) {
        Tcl_Panic("Ns_TclSetAddrObj called with shared object");
    }
    Ns_TclSetTwoPtrValue(objPtr, &addrType, (void *) type, addr);
    Tcl_InvalidateStringRep(objPtr);
}


/*
 *----------------------------------------------------------------------
 *
 * Ns_TclGetOpaqueFromObj --
 *
 *      Stores in the address given by addrPtrPtr the internal pointer of an
 *      address Tcl_Obj.
 *
 * Results:
 *      TCL_OK or TCL_ERROR if object can not be interpreted as an address.
 *
 * Side effects:
 *      None.
 *
 *----------------------------------------------------------------------
 */

int
Ns_TclGetOpaqueFromObj(const Tcl_Obj *objPtr, const char *type, void **addrPtrPtr)
{
    int result = TCL_OK;

    NS_NONNULL_ASSERT(objPtr != NULL);
    NS_NONNULL_ASSERT(type != NULL);
    NS_NONNULL_ASSERT(addrPtrPtr != NULL);

    if (objPtr->typePtr == &addrType
        && objPtr->internalRep.twoPtrValue.ptr1 == (void *) type) {
        *addrPtrPtr = objPtr->internalRep.twoPtrValue.ptr2;
    } else {
        char      s[33] = {0};
        uintptr_t t = 0u, a = 0u;

        if ((sscanf(Tcl_GetString((Tcl_Obj *) objPtr), "t%20" SCNxPTR "-a%20" SCNxPTR "-%32s", &t, &a, s) != 3)
            || (strcmp(s, type) != 0)
            || (t != (uintptr_t)type)
            ) {
            result = TCL_ERROR;
        } else {
            *addrPtrPtr = (void *)a;
        }
    }

    return result;
}


/*
 *----------------------------------------------------------------------
 *
 * Ns_TclSetOpaqueObj --
 *
 *      Converts the Tcl_Obj, which may be shared, to the ns:addr type without
 *      invalidating the current string representation,  and sets the internal
 *      representation.
 *
 * Results:
 *      None.
 *
 * Side effects:
 *      None.
 *
 *----------------------------------------------------------------------
 */

void
Ns_TclSetOpaqueObj(Tcl_Obj *objPtr, const char *type, void *addr)
{
    NS_NONNULL_ASSERT(objPtr != NULL);
    NS_NONNULL_ASSERT(type != NULL);

    Ns_TclSetTwoPtrValue(objPtr, &addrType, (void *) type, addr);
}


/*
 *----------------------------------------------------------------------
 *
 * NsTclObjIsByteArray --
 *
 *      Determines whether the given object has a byte array internal
 *      representation that accurately represents any existing string
 *      representation.  If so, the byte array can be used directly.  This is
 *      needed because unfortunately, when creating byte array, if Tcl
 *      encounters a character whose code point is > 255, it uses only the
 *      lower byte as the value of the character.
 *
 * Results:
 *      Boolean.
 *
 * Side effects:
 *      None.
 *
 *----------------------------------------------------------------------
 */

bool
NsTclObjIsByteArray(const Tcl_Obj *objPtr)
{
    bool result;

    NS_NONNULL_ASSERT(objPtr != NULL);

    /*
     * This resembles the tclInt.h function for testing pure byte arrays. In
     * versions up to at least on Tcl 8.6, a byte array was considered pure
     * only if it there was no string representation.  Starting with Tcl 8.7a1,
     * The internal properByteArrayTypePtr type indicates that the string
     * representation, if any, does not contain characters that would be
     * truncated to create the byte array internal representation.
     */
#ifdef NS_TCL_PRE87
    result = ((objPtr->typePtr == byteArrayTypePtr) && (objPtr->bytes == NULL));
#else
    result = (objPtr->typePtr == properByteArrayTypePtr);
#endif

#if 0
    fprintf(stderr, "NsTclObjIsByteArray? %p type %p proper %d old %d bytes %p name %s => %d\n",
            (void*)objPtr,
            (void*)(objPtr->typePtr),
            (objPtr->typePtr == properByteArrayTypePtr),
            (objPtr->typePtr == byteArrayTypePtr),
            (void*)(objPtr->bytes),
            objPtr->typePtr == NULL ? "string" : objPtr->typePtr->name,
            result);
#endif

    return result;
}


/*
 *----------------------------------------------------------------------
 *
 * NsTclObjIsEncodedByteArray --
 *
 *      Obsolete. Use NsTclObjIsByteArray instead.
 *
 * Results:
 *      Boolean.
 *
 * Side effects:
 *      None.
 *
 *----------------------------------------------------------------------
 */

bool
NsTclObjIsEncodedByteArray(const Tcl_Obj *objPtr)
{
    NS_NONNULL_ASSERT(objPtr != NULL);

    return ((objPtr->typePtr == byteArrayTypePtr) && (objPtr->bytes != NULL));
}


/*
 *----------------------------------------------------------------------
 *
 * UpdateStringOfAddr --
 *
 *      Updates the string representation for an address object.  Does not free
 *      an existing old string representation so storage is be lost if this has
 *      not already been done.
 *
 * Results:
 *      None.
 *
 * Side effects:
 *      None.
 *
 *----------------------------------------------------------------------
 */

static void
UpdateStringOfAddr(Tcl_Obj *objPtr)
{
    const char *type = objPtr->internalRep.twoPtrValue.ptr1;
    const void *addr = objPtr->internalRep.twoPtrValue.ptr2;
    char        buf[128];
    int         len;

    len = snprintf(buf, sizeof(buf), "t%" PRIxPTR "-a%" PRIxPTR "-%s", (uintptr_t)type, (uintptr_t)addr, type);
    Ns_TclSetStringRep(objPtr, buf, len);
}


/*
 *----------------------------------------------------------------------
 *
 * SetAddrFromAny --
 *
 *      Attempts to create an address internal representation of the Tcl_Obj.
 *
 * Results:
 *      A standard Tcl result.  If an error occurs during conversion and interp
 *      is not null, sets the interpreter result to an error message.
 *
 * Side effects:
 *      None.
 *
 *----------------------------------------------------------------------
 */

static int
SetAddrFromAny(Tcl_Interp *interp, Tcl_Obj *objPtr)
{
    int   result = TCL_OK;
    void *type, *addr;
    char *chars;

    chars = Tcl_GetString(objPtr);
    if ((sscanf(chars, "t%20p-a%20p", &type, &addr) != 2)
        || (type == NULL)
        || (addr == NULL)
        ) {
        Ns_TclPrintfResult(interp, "invalid address \"%s\"", chars);
        result = TCL_ERROR;
    } else {
        Ns_TclSetTwoPtrValue(objPtr, &addrType, type, addr);
    }

    return result;
}


/*
 * Local Variables:
 * mode: c
 * c-basic-offset: 4
 * fill-column: 78
 * indent-tabs-mode: nil
 * End:
 */
