/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 * http://mozilla.org/.
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See
 * the License for the specific language governing rights and limitations
 * under the License.
 *
 * The Original Code is AOLserver Code and related documentation
 * distributed by AOL.
 *
 * The Initial Developer of the Original Code is America Online,
 * Inc. Portions created by AOL are Copyright (C) 1999 America Online,
 * Inc. All Rights Reserved.
 *
 * Alternatively, the contents of this file may be used under the terms
 * of the GNU General Public License (the "GPL"), in which case the
 * provisions of GPL are applicable instead of those above.  If you wish
 * to allow use of your version of this file only under the terms of the
 * GPL and not to allow others to use your version of this file under the
 * License, indicate your decision by deleting the provisions above and
 * replace them with the notice and other provisions required by the GPL.
 * If you do not delete the provisions above, a recipient may use your
 * version of this file under either the License or the GPL.
 */


/*
 * str.c --
 *
 *      Functions that deal with strings.
 */

#include "nsd.h"


/*
 *----------------------------------------------------------------------
 *
 * Ns_StrTrim --
 *
 *      Trims leading and trailing white space from a string.
 *
 * Results:
 *      A pointer to the trimmed string, which will be in the original
 *      string.
 *
 * Side effects:
 *      May modify passed-in string.
 *
 *----------------------------------------------------------------------
 */

char *
Ns_StrTrim(char *chars)
{
    NS_NONNULL_ASSERT(chars != NULL);

    return Ns_StrTrimLeft(Ns_StrTrimRight(chars));
}



/*
 *----------------------------------------------------------------------
 *
 * Ns_StrTrimLeft --
 *
 *      Trims leading white space from a string.
 *
 * Results:
 *      A pointer to the trimmed string somewhere in the original string.
 *
 * Side effects:
 *      None.
 *
 *----------------------------------------------------------------------
 */

char *
Ns_StrTrimLeft(char *chars)
{
    NS_NONNULL_ASSERT(chars != NULL);

    while (CHARTYPE(space, *chars) != 0) {
        ++chars;
    }

    return chars;
}



/*
 *----------------------------------------------------------------------
 *
 * Ns_StrTrimRight --
 *
 *      Trims trailing white space from a string.
 *
 * Results:
 *      A pointer to the trimmed string somewhere in the original string.
 *
 * Side effects:
 *      The string will be modified.
 *
 *----------------------------------------------------------------------
 */

char *
Ns_StrTrimRight(char *chars)
{
    int len;

    NS_NONNULL_ASSERT(chars != NULL);

    len = (int)strlen(chars);

    while ((--len >= 0)
           && (CHARTYPE(space, chars[len]) != 0
               || chars[len] == '\n')) {
        chars[len] = '\0';
    }
    return chars;
}


/*
 *----------------------------------------------------------------------
 *
 * Ns_StrToLower --
 *
 *      Converts all uppercase characters in a string to lowercase.
 *
 * Results:
 *      The same pointer that was passed in.
 *
 * Side effects:
 *      Modifies the string.
 *
 *----------------------------------------------------------------------
 */

char *
Ns_StrToLower(char *chars)
{
    char *p;

    NS_NONNULL_ASSERT(chars != NULL);

    p = chars;
    while (*p != '\0') {
        if (CHARTYPE(upper, *p) != 0) {
            *p = CHARCONV(lower, *p);
        }
        ++p;
    }
    return chars;
}


/*
 *----------------------------------------------------------------------
 *
 * Ns_StrToUpper --
 *
 *      Converts all lowercase characters in a string to uppercase.
 *
 * Results:
 *      The same pointer that was passed in.
 *
 * Side effects:
 *      Modifies the string.
 *
 *----------------------------------------------------------------------
 */

char *
Ns_StrToUpper(char *chars)
{
    char *s;

    NS_NONNULL_ASSERT(chars != NULL);

    s = chars;
    while (*s != '\0') {
        if (CHARTYPE(lower, *s) != 0) {
            *s = CHARCONV(upper, *s);
        }
        ++s;
    }
    return chars;
}


/*
 *----------------------------------------------------------------------
 *
 * Ns_StrToInt --
 *
 *      Converts a string to an integer.
 *
 *      The string, which may begin with an abritray amount of whitespace, as
 *      determined by isspace(3), contains a single optional `+' or `-' sign,
 *      and either a hexadecimal representation beginning with `0x', or a
 *      decimal representation.
 *
 * Results:
 *      NS_OK, or NS_ERROR if the number cannot be parsed or overflows.
 *
 * Side effects:
 *      Stores the integer in the pointer given by intPtr.
 *
 *----------------------------------------------------------------------
 */

Ns_ReturnCode
Ns_StrToInt(const char *chars, int *intPtr)
{
    long          lval;
    char         *ep;
    Ns_ReturnCode status = NS_OK;

    NS_NONNULL_ASSERT(chars != NULL);
    NS_NONNULL_ASSERT(intPtr != NULL);

    errno = 0;
    lval = strtol(chars, &ep, chars[0] == '0' && chars[1] == 'x' ? 16 : 10);
    if (unlikely(chars[0] == '\0' || *ep != '\0')) {
        status = NS_ERROR;
    } else {
        if (unlikely((errno == ERANGE && (lval == LONG_MAX || lval == LONG_MIN))
                     || (lval > INT_MAX || lval < INT_MIN))) {
            status = NS_ERROR;
        } else {
            *intPtr = (int) lval;
        }
    }

    return status;
}

/*
 *----------------------------------------------------------------------
 *
 * Ns_StrToWideInt --
 *
 *      Converts a string to a wide integer.
 *
 *      The string contains optional whitespace as determined by isspace(3)
 *      followed by a single optional `+' or `-' sign, an optional prefix of
 *      `0x' indicating a number in base 16, and the number, which by default
 *      is a decimal number.
 *
 * Results:
 *      NS_OK, or NS_ERROR if the number cannot be parsed or overflows.
 *
 * Side effects:
 *      Stores the wide integer at the address pointed to by intPtr.
 *
 *----------------------------------------------------------------------
 */

Ns_ReturnCode
Ns_StrToWideInt(const char *chars, Tcl_WideInt *intPtr)
{
    Tcl_WideInt   lval;
    char         *ep;
    Ns_ReturnCode status = NS_OK;

    errno = 0;
    lval = strtoll(chars, &ep, chars[0] == '0' && chars[1] == 'x' ? 16 : 10);
    if (unlikely(chars[0] == '\0' || *ep != '\0')) {
        status = NS_ERROR;
    } else {
        if (unlikely(errno == ERANGE && (lval == LLONG_MAX || lval == LLONG_MIN))) {
            status = NS_ERROR;
        } else {
            *intPtr = (Tcl_WideInt) lval;
        }
    }

    return status;
}

/*
 *----------------------------------------------------------------------
 *
 * Ns_StrToMemUnit --
 *
 *      Converts a string giving a memory size as a decimal number followed by
 *      kB, MB, GB, KiB, MiB, or GiB, to an integer indicating the size in
 *      bytes. 
 *
 *      The string contains optional whitespace as determined by isspace(3)
 *      followed by a decimal nunber and an optional `.' followed by a decimal
 *      fraction part.
 *
 * Results:
 *      NS_OK, or NS_ERROR if the number cannot be parsed or overflows.
 *
 * Side effects:
 *      Stores the resulting value at the address given by intPtr.
 *
 *----------------------------------------------------------------------
 */
Ns_ReturnCode
Ns_StrToMemUnit(const char *chars, Tcl_WideInt *intPtr)
{
    Ns_ReturnCode status = NS_OK;

    if (chars[0] == '\0') {
        *intPtr = (Tcl_WideInt) 0;

    } else {
        Tcl_WideInt lval;
        char       *endPtr;

        /*
         * Parse the first part of the number.
         */
        errno = 0;
        lval = strtoll(chars, &endPtr, 10);
        if (unlikely(errno == ERANGE && (lval == LLONG_MAX || lval == LLONG_MIN))) {
            /*
             * strtoll() parsing failed.
             */
            status = NS_ERROR;
        } else {
            int    multiplier = 1;
            double fraction = 0.0;

            if (*endPtr != '\0') {
                /*
		 * Not the end of the string. Check for decimal digits.
                 */
                if (*endPtr == '.') {
                    long long decimal;
                    long      i;
                    ptrdiff_t digits;
                    int       divisor = 1;
                    char     *ep;

                    endPtr++;
                    decimal = strtoll(endPtr, &ep, 10);
                    digits = ep-endPtr;
                    for (i = 0; i < digits; i++) {
                        divisor *= 10;
                    }
                    fraction = (double)decimal / (double)divisor;
                    endPtr = ep;
                }
                /*
                 * Skip whitespace.
                 */
                while (CHARTYPE(space, *endPtr) != 0) {
                    endPtr++;
                }
                /*
                 * Parse memory units.
                 *
                 * The International System of Units (SI) defines
                 *    kB, MB, GB as 1000, 1000^2, 1000^3 bytes,
                 * and IEC defines
                 *    KiB, MiB and GiB as 1024, 1024^2, 1024^3 bytes.
                 *
                 * For effective memory usage, multiple of 1024 are
                 * better. Therefore, follow the PostgreSQL conventions and
                 * use 1024 as the multiplier for both SI and IEC 
                 * units.
                 */
                if (*endPtr == 'M' && *(endPtr+1) == 'B') {
                    multiplier = 1024 * 1024;
                } else if ((*endPtr == 'K' || *endPtr == 'k') && *(endPtr+1) == 'B') {
                    multiplier = 1024;
                } else if (*endPtr == 'G' && *(endPtr+1) == 'B') {
                    multiplier = 1024 * 1024 * 1024;

                } else if (*endPtr == 'M' && *(endPtr+1) == 'i' && *(endPtr+2) == 'B') {
                    multiplier = 1024 * 1024;
                } else if ((*endPtr == 'K') && *(endPtr+1) == 'i' && *(endPtr+2) == 'B') {
                    multiplier = 1024;
                } else if (*endPtr == 'G' && *(endPtr+1) == 'i' && *(endPtr+2) == 'B') {
                    multiplier = 1024 * 1024 * 1024;
                } else {
                    status = NS_ERROR;
                }
            }
            if (status == NS_OK) {
                /*
                 * The input was successfully parsed.
                 */
                if (fraction > 0.0) {
                    /*
                     * Compute a total including the fraction part.
                     */
                    double r = (double)(lval * multiplier) + fraction * multiplier;

                    *intPtr = (Tcl_WideInt)r;
                } else {
                    /*
                     * Compute a total.
                     */
                    *intPtr = (Tcl_WideInt) lval * multiplier;
                }
            }
        }
    }

    return status;
}


/*
 *----------------------------------------------------------------------
 *
 * Ns_Match --
 *
 *      Compares the beginnings of two strings case insensitively.
 *      The comparison stops when the end of the shorter string is
 *      reached.
 *
 * Results:
 *      NULL if the strings don't match.
 *
 * Side effects:
 *      None.
 *
 *----------------------------------------------------------------------
 */

const char *
Ns_Match(const char *a, const char *b)
{
    if (a != NULL && b != NULL) {
        while (*a != '\0' && *b != '\0') {
            char c1 = (CHARTYPE(lower, *a) != 0) ? *a : CHARCONV(lower, *a);
            char c2 = (CHARTYPE(lower, *b) != 0) ? *b : CHARCONV(lower, *b);
            if (c1 != c2) {
                b = NULL;
                break;
            }
            a++;
            b++;
        }
    }
    return b;
}


/*
 *----------------------------------------------------------------------
 *
 * Ns_NextWord --
 *
 *        Returns a pointer to the first non-whitespace character that follows
 *        whitespace in a string, or a pointer to the final null.
 *
 * Results:
 *        A string pointer in the original string.
 *
 * Side effects:
 *        None.
 *
 *----------------------------------------------------------------------
 */

const char *
Ns_NextWord(const char *line)
{
    while (*line != '\0' && CHARTYPE(space, *line) == 0) {
        ++line;
    }
    while (*line != '\0' && CHARTYPE(space, *line) != 0) {
        ++line;
    }
    return line;
}


/*
 *----------------------------------------------------------------------
 *
 * Ns_StrCaseFind --
 *
 *      Finds the first occurrence of a substring, case insensitively, within a
 *      string.
 *
 * Results:
 *      A pointer to the substring in the string or NULL.
 *
 * Side effects:
 *      None.
 *
 *----------------------------------------------------------------------
 */

const char *
Ns_StrNStr(const char *chars, const char *subString)
{
    NS_NONNULL_ASSERT(chars != NULL);
    NS_NONNULL_ASSERT(subString != NULL);

    return Ns_StrCaseFind(chars, subString);
}

const char *
Ns_StrCaseFind(const char *chars, const char *subString)
{
    const char *result = NULL;

    NS_NONNULL_ASSERT(chars != NULL);
    NS_NONNULL_ASSERT(subString != NULL);

    if (strlen(chars) > strlen(subString)) {
        while (*chars != '\0') {
            if (Ns_Match(chars, subString) != NULL) {
                result = chars;
                break;
            }
            ++chars;
        }
    }
    return result;
}


/*
 *----------------------------------------------------------------------
 *
 * Ns_StrIsValidHostHeaderContent --
 *
 *      Determines whether a string contains only characters permitted in a
 *      Host header: Letters, digits, single periods and the colon port
 *      separator.
 *
 * Results:
 *      NS_TRUE or NS_FALSE.
 *
 * Side effects:
 *      None.
 *
 *----------------------------------------------------------------------
 */

bool
Ns_StrIsValidHostHeaderContent(const char *chars)
{
    register const char *p;
    bool result = NS_TRUE;

    NS_NONNULL_ASSERT(chars != NULL);

    for (p = chars; *p != '\0'; p++) {
        if ((CHARTYPE(alnum, *p) == 0) && (*p != ':')
            && (*p != '[') && (*p != ']')                           /* IP-literal notation */
            && ((*p != '.') || (p[0] == '.' && p[1] == '.'))) {

            result = NS_FALSE;
            break;
        }
    }

    return result;
}


/*
 *----------------------------------------------------------------------
 *
 * Ns_GetBinaryString --
 *
 *      Encodes the string representation of a Tcl_Obj into utf-8 if it is not
 *      certain that the string only contains characters having a code point of
 *      255 or less, or if forceBinary is true, returns the byterrary for
 *      Tcl_Obj.
 *
 * Results:
 *      Content of the Tcl_Obj.
 *
 * Side effects:
 *      None.
 *
 *----------------------------------------------------------------------
 */
const unsigned char *
Ns_GetBinaryString(Tcl_Obj *obj, bool forceBinary, int *lengthPtr, Tcl_DString *dsPtr)
{
    const unsigned char *result;

    NS_NONNULL_ASSERT(obj != NULL);
    NS_NONNULL_ASSERT(lengthPtr != NULL);

    /*
     * When performing binary encoding Tcl does not first encode the string
     * into utf-8. It instead uses only the lower byte of unicode code point
     * for each character.  For example the character "ü" has a unicode code
     * point that fits into one byte, but its utf-8 encoding is 2 bytes:
     *
     * % binary encode hex ü
     * fc
     * % binary encode hex [encoding convertto utf-8 ü]
     * c3bc
     *
     * This is true for base64 as well. The casual user expects the behavior
     * that occurs in a shell session, where pipelines encode data into the
     * system encoding, which is typically utf-8:
     *
     * $ echo -n "ü" |base64
     * w7w=
     *
     * % ns_base64encode ü
     * w7w=
     *
     * In order to get the same result in Tcl, one must perform the
     * utf-conversion "manually":
     *
     * % binary encode base64 ü
     * /A==
     *
     * % binary encode base64 [encoding convertto utf-8 ü]
     * w7w=
     *
     * Th Naviserver crypto commands should automatically encode values to
     * utf-8 so to conform to shell pipeline behaviour so that the user isn't
     * burdened with making this conversion:
     *
     * $ echo -n "ü" |openssl sha1
     * (stdin)= 94a759fd37735430753c7b6b80684306d80ea16e
     *
     * % ns_sha1 "ü"
     * 94A759FD37735430753C7B6B80684306D80EA16E
     *
     * % ns_md string -digest sha1 ü
     * 94a759fd37735430753c7b6b80684306d80ea16e
     *
     * The same should hold as well for 3-byte UTF-8 characters
     *
     * $ echo -n "☀" |openssl sha1
     * (stdin)= d5b6c20ee0b3f6dafa632a63eafe3fd0db26752d
     *
     * % ns_sha1 ☀
     * D5B6C20EE0B3F6DAFA632A63EAFE3FD0DB26752D
     *
     * % ns_md string -digest sha1 ☀
     * d5b6c20ee0b3f6dafa632a63eafe3fd0db26752d
     *
     */

    if (forceBinary || NsTclObjIsByteArray(obj)) {
        //fprintf(stderr, "NsTclObjIsByteArray\n");
        result = (unsigned char *)Tcl_GetByteArrayFromObj(obj, lengthPtr);
    } else {
        int         stringLength;
        const char *charInput;

        charInput = Tcl_GetStringFromObj(obj, &stringLength);

        //if (NsTclObjIsEncodedByteArray(obj)) {
        //    fprintf(stderr, "NsTclObjIsEncodedByteArray\n");
        //} else {
        //    //fprintf(stderr, "some other obj\n");
        //}

        Tcl_UtfToExternalDString(NS_utf8Encoding, charInput, stringLength, dsPtr);
        result = (unsigned char *)dsPtr->string;
        *lengthPtr = dsPtr->length;
    }

    return result;
}


/*
 * Local Variables:
 * mode: c
 * c-basic-offset: 4
 * fill-column: 78
 * indent-tabs-mode: nil
 * End:
 */
