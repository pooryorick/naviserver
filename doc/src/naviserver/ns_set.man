[include version_include.man]
[manpage_begin ns_set n [vset version]]
[moddesc {NaviServer Built-in Commands}]

[titledesc {Manipulate sets of key-value pairs}]

[description]

ns_set manages an orderd set of fields where each field has a name and a
value.  The index of the first field is 0.  Names are not required to be
unique, which is useful, e.g. for HTTP headers which have this same property.

The options, which may be abbreviated, are:

[section {COMMANDS}]

[list_begin definitions]


[call [cmd ns_set] [opt [arg {arg arg ...}]]]


[list_end]

[list_begin definitions]


[call [cmd "ns_set array"] [arg setId]]

Returns as a dictionary the contents of the the set referenced by ''setId'' .
As with all dictionaries, if this dictionary is converted to an array, any
values with duplicate keys are discarded.

[call [cmd "ns_set cleanup"]]

Deletes all sets created via [cmd ns_set].  [cmd ns_cleanup] runs after every
request and executes this routine.

[call [cmd "ns_set copy"] [arg setId]]

Returns a new set that has the same name and contents as the set referenced in
setId.  The new set is automatically deleted when the transaction ends.

[call [cmd "ns_set cput"] [arg setId] [arg key] [arg value]]

Conditional put.  Appends to the set a field composed of key and value if there
is no field in the set with that key.  Returns the field number of the new
field, or the field number of the existing field.

[call [cmd "ns_set create"] [opt [arg name]] [opt [arg key]] [opt [arg value]] ...]

Allocates a new set and returns the setId for it.  
The new set is automatically deleted when the transaction ends.

[call [cmd "ns_set delete"] [arg setId] [arg index]]

Deletes the field at index.

[call [cmd "ns_set delkey"] [arg setId] [arg key]]

Deletes the first field in the set having the specified key.  Remaining fields
having the same name are not touched.

[call [cmd "ns_set find"] [arg setId] [arg key]]

Returns the index of the first field in the set having the specified key,
or -1 if there is no such field.

[call [cmd "ns_set free"] [arg setId]]

Deletes the specified set. Sets are automatically deleted when the transaction
ends, but in a loop with many iterations it might be useful to delete a set
manually.

[call [cmd "ns_set get"] [arg setId] [arg key] [opt [arg default]]]

Returns the first value associated with key.  If key isn't in the set,
returns an emptry string, or the default if provided.

[call [cmd "ns_set icput"] [arg setId] [arg key] [arg value]]

Case-insensitive counterpart of [cmd "ns_set cput"].

[call [cmd "ns_set idelkey"] [arg setId] [arg key]]

Case-insensitive counterpart of [cmd "ns_set delkey"].

[call [cmd "ns_set ifind"] [arg setId] [arg key]]

Case-insensitive counterpart of [cmd "ns_set find"].

[call [cmd "ns_set iget"] [arg setId] [arg key] [opt [arg default]]]

Case-insensitive counterpart of [cmd "ns_set get"].

[call [cmd "ns_set imerge"] [arg high] [arg low]]

Merges two sets. This is the case-insensitive version of
[cmd "ns_set merge"].

[call [cmd "ns_set isnull"] [arg setId] [arg index]]

Returns 1 if the value of the field specified by index is null
and 0 if it is not.  An empty string is not considered to be null.

[call [cmd "ns_set iunique"] [arg setId] [arg key]]

Case-insensitive counterpart of [cmd "ns_set unique"].

[call [cmd "ns_set iupdate"] [arg setId] [arg key] [arg value]]

Case-insensitive counterpart of [cmd "ns_set update"].


[call [cmd "ns_set key"] [arg setId] [arg index]]

Returns the key for the field at index.  Use this routine to iterate through
each field in the set.


[call [cmd "ns_set keys"] [arg setId] [opt [arg pattern]]]

Returns a list of all keys in the given ns_set.
If a pattern is given, returns only those keys that match it
according to Tcl's string match.



[call [cmd "ns_set list"] ]

Returns a list of all ns_sets. 

[call [cmd "ns_set merge"] [arg high] [arg low]]

Merges two sets.  Each field in the low set is appended to the high set if
there is no matching field having that key in the high set.

[call [cmd "ns_set move"] [arg to] [arg from]]

Moves all fields to the first set from the second set, leaving the second set
set empty.

[call [cmd "ns_set name"] [arg setId]]

Returns the name of the set or the empty string if the set has no name.

[call [cmd "ns_set print"] [arg setId]]

Prints the specified set to stderr, which is typically the server log.  
Useful for debugging.

[call [cmd "ns_set put"] [arg setId] [arg key] [arg value]]

Appends to the set a new field composed of key and value.  Existing fields by
the same name remain in place.

[call [cmd "ns_set size"] [arg setId]]

Returns the number of fields in the set.

[call [cmd "ns_set split"] [arg setId] [opt [arg splitChar]]]

Splits one set into multiple sets.  In each original key, the string following
splitChar identifies the new set to append the field to.  The new key is the
string before splitChar key.  Returns a list of identifiers for the new sets.
The default splitChar is a period ".".

[call [cmd "ns_set truncate"] [arg setId] [arg index]]

Removes the field at index and subsequent fields.

[call [cmd "ns_set unique"] [arg setId] [arg key]]

Returns 1 if the key is unique in the set, and 0 otherwise.  The test is
case-sensitive.  Useful, for when working with HTTP headers, which are not
necessarily uniquely named.  For example, the HTTP "Accept" key often occurs
multiple times.

[call [cmd "ns_set update"] [arg setId] [arg key] [arg value]]

Deletes the first field matching key if there is one and then appends to the
set the field composed of key and value.

[call [cmd "ns_set value"] [arg setId] [arg index]]

Returns the value of the set at index.  Use this routine to 
iterate through the values in the set.

[call [cmd "ns_set values"] [arg setId] [opt [arg pattern]]]

Returns a list of all values in the given ns_set.
If a pattern is supplied, only those values that match it
(according to the Tcl rules of string match) will be returned.


[list_end]

[section EXAMPLES]

[example_begin]
 % set mySet [lb]ns_set create mySetName a b c d e f A Joe B John C Jeff[rb]
 d0
[example_end]

[example_begin]
 % ns_set size $mySet
 6
[example_end]

[example_begin]
 % ns_set name $mySet
 mySetName
[example_end]

[example_begin]
 % ns_set array $mySet
 a b c d e f A Joe B John C Jeff
[example_end]

[example_begin]
 % ns_set keys $mySet
 a c e A B C
[example_end]

[example_begin]
 % ns_set values $mySet
 b d f Joe John Jeff
[example_end]


[example_begin]
 % ns_set get $mySet A
 Joe
[example_end]

[example_begin]
 % ns_set iget $mySet a
 b
[example_end]

[example_begin]
 % ns_set unique $mySet a
 1
[example_end]

[example_begin]
 % ns_set iunique $mySet a
 0
[example_end]

[example_begin]
 % ns_set truncate $mySet 3
 
 % ns_set print $mySet
 mySetName:
      a = b
      c = d
      e = f
[example_end]

[example_begin]
 % ns_set update $mySet c "Hello World!"
 2
[example_end]

[example_begin]
 % ns_set print $mySet
 mySetName:
      a = b
      e = f
      c = Hello World!
[example_end]

[example_begin]
 % ns_set find $mySet c
 2
[example_end]

[example_begin]
 % ns_set find $mySet nokey
 -1
[example_end]

[example_begin]
 % ns_set delete $mySet 0
[example_end]

[example_begin]
 % ns_set array $mySet
 e f c {Hello World!}
[example_end]

[para]

[example_begin]
 % set anotherSet [lb]ns_set create[rb]
 d1
[example_end]

[example_begin]
 % ns_set list
 d0 d1
[example_end]

[example_begin]
 % ns_set put $anotherSet dog.food "Yummy dog food!"
 0
[example_end]

[example_begin]
 % ns_set put $anotherSet cat.food "Yummy cat food!"
 1
[example_end]

[example_begin]
 % ns_set print $anotherSet
      dog.food = Yummy dog food!
      cat.food = Yummy cat food!
[example_end]

[example_begin]
 % set newSets [lb]ns_set split $anotherSet[rb]
 d2 d3
[example_end]

[para]

[example_begin]
 % foreach s $newSets { ns_set print $s }
 dog:
      food = Yummy dog food!
 cat:
      food = Yummy cat food!
[example_end]

[example_begin]
 % ns_set key $anotherSet 0
 dog.food
[example_end]

[example_begin]
 % ns_set value $anotherSet 1
 Yummy cat food!
[example_end]

[example_begin]
 % ns_set move $mySet $anotherSet
 d0
[example_end]

[example_begin]
 % ns_set array $mySet
 e f c {Hello World!} dog.food {Yummy dog food!} cat.food {Yummy cat food!}
[example_end]

[example_begin]
 % ns_set array $anotherSet
[example_end]

[example_begin]
 % set thirdSet [lb]ns_set new[rb]
 d4
[example_end]

[example_begin]
 % ns_set move $thirdSet $mySet
 d4
[example_end]

[example_begin]
 % ns_set array $thirdSet
 e f c {Hello World!} dog.food {Yummy dog food!} cat.food {Yummy cat food!}
[example_end]

[example_begin]
 % array set testArray [lb]ns_set array $thirdSet[rb]
[example_end]

[example_begin]
 # to run through an ns_set
 for {set i 0} {$i < [lb]ns_set size $myset[rb]} {incr i} {
   set key [lb]ns_set key $myset $i[rb]
   set value [lb]ns_set value $myset $i[rb]
 }
[example_end]

[section {NOTES}]

[para]
A NULL value, which is distinct from and empty string, is useful when
representing a database row as an [cmd ns_set].  Currently, it is difficult to
get a NULL value in an [cmd ns_set] through the Tcl API.  [cmd ns_parsequery]
may insert a NULL value, and omitting the last value when creating the ns_set
may result in a NULL value.  The C API provides the means to add NULL values.

[para]
[cmd ns_set] is intended for relatively small amounts of data:  Keys are found
by a linear search through the whole underlying C array.  Adding large numbers
of fields to a set degrades performance Use a Tcl Array or a Tcl dict instead.

[see_also ns_findset ns_ictl ns_cleanup]
[keywords "global built-in" ns_set "data structure"]

[manpage_end]


